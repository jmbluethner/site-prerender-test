import * as importer from '../../js/importer.js';

const footer = {
    data: function () {
        return {

        }
    },
    template: `
        <footer>
            <!--
            <div class="top_area">
                <span>Developed by ... Myself. Sick, Right?</span>
            </div>
            -->       
            <div class="lower_area">
                <div class="block">
                    <h6>Legal</h6>
                    <ul>
                        <a href="/datenschutz"><li>Data protection</li></a>
                        <a href="/haftungsausschluss"><li>Disclaimer</li></a>
                        <a href="/impressum"><li>Imprint</li></a>
                    </ul>
                </div>
                <div class="block">
                    <h6>Contact</h6>
                    <span>
                        Feel free to reach out to me via the <a href="#contact">contact form</a>.
                    </span>
                </div>
                <div class="block social">
                    <h6>See also</h6>
                    <a target="_blank" href="https://instagram.com/heliophobix/"><i class="fab fa-instagram"></i></a>
                    <a target="_blank" href="https://facebook.com/Heliophobix-Media-171785301290717/"><i class="fab fa-facebook"></i></a>
                    <a href="https://www.youtube.com/c/HeliophobixMedia" target="_blank"><i class="fab fa-youtube"></i></a>
                </div>
                <div class="block">
                    <h6>Shop</h6>
                    <span>So you want some Lightroom Presets? Great! Check out <a href="https://shop.heliophobix.com" target="_blank">shop.heliophobix.com</a><br></span>
                </div>
            </div>
            <hr>
            <div class="bottom_area">
                <span>&copy; 2022 Heliophobix Media</span>
            </div>
        </footer>
    `
}

// Load the components CSS
importer.appendCssFile('/views/components/footer.css',function () {})

// Export the component
export {footer}