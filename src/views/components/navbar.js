import * as importer from '../../js/importer.js';

const navbar = {
    template: `
        <div id="navbar_wrapper">
            <div id="navbar_container">
                <div class="inner">
                    Navbar
                </div>
            </div>
        </div>
    `
}

// Load the components CSS
importer.appendCssFile('/views/components/navbar.css',function () {})

// Export the component
export {navbar}