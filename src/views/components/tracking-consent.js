import * as importer from '../../js/importer.js';
import {getCookieByName, setCookie} from "../../js/cookie-handler.js";
import {appendJsFile} from "../../js/importer.js";

const trackingConsent = {
    data: function () {
        return {
            showTrackingOverlay: false
        }
    },
    methods: {
        checkConsent: function() {
            let vue = this;
            if(getCookieByName('tracking_consent') === 'allow') {
                vue.showTrackingOverlay = false;
            } else vue.showTrackingOverlay = getCookieByName('tracking_consent') !== 'deny';
        },
        setConsentDeny: function () {
            setCookie('tracking_consent','deny','90');
            this.showTrackingOverlay = false;
        },
        setConsentAllow: function () {
            setCookie('tracking_consent','allow','90');
            this.showTrackingOverlay = false;
        }
    },
    mounted: function () {
        this.checkConsent();
    },
    template: `
        <div v-if="showTrackingOverlay" id="tracking_overlay_container">
            <div class="tracking_overlay_inner">
                <div class="text">
                    <span>Asking for your consent</span>
                    <p>
                    Some consent text which is German on the original page so I just replaced it with this spacer. If you've read this until this point, you are a true G. Cheers.
                    </p>
                </div>
                <div class="button_container">
                    <button @click="setConsentAllow()">Accept</button>
                    <button @click="setConsentDeny()">Deny</button>
                </div>
            </div>
        </div>
    `
}

// Load the components CSS
importer.appendCssFile('/views/components/tracking-consent.css',function () {})

// Export the component
export {trackingConsent}