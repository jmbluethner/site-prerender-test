// Import js
import {logToConsole} from "../../js/logger.js"

// Import Vue Loader
import {prepareVue} from "../main.js";

// Import components
import {navbar} from "../components/navbar.js";
import {footer} from "../components/footer.js";
import {trackingConsent} from "../components/tracking-consent.js";

// Main Component
prepareVue(function () {
    logToConsole('Initializing Vue views...','info');

    let bus = {};
    bus.eventBus = new Vue();

    // Create app
    let app = new Vue({
        // Use div#app as container
        el: '#app',
        // define all components
        components: {
            navbar: navbar,
            customfooter: footer,
            trackingConsent: trackingConsent
        },
        // build the template
        template:
            `
            <div>
                <navbar />
                <customfooter />
                <trackingConsent />
            </div>
            `
    });
})