import * as importer from '../js/importer.js';


export function prepareVue(callback) {

        importer.appendJsFile('../lib/vue/vue.js','text/javascript', function () {
            callback();
        });

}