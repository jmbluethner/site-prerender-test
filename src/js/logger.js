/**
 * @description Returns a formatted timestamp for the logToConsole() function
 * @return {string}
 */
function logTimestamp() {
    let date = new Date().toLocaleDateString();
    let time = new Date().toLocaleTimeString();
    return '['+date+' | '+time+'] ';
}

const applicationPrefix = 'site-prerender-test ~> ';

/**
 * @description This function can be used to send output to the console. It only logs if debugMode in applicationSettings is enabled.
 * @function logToConsole
 * @param {object} msg Whatever you want to log
 * @param {string} logType Can be 'info', 'warning', 'success', or 'error'.
 */
export function logToConsole(msg,logType) {

    if(logType === 'error') {
        console.log('%c'+logTimestamp()+applicationPrefix+'%c'+msg,'background: rgba(255,0,0,.4); color: rgba(255,255,255,.4); font-size: 12px; line-height: 22px; padding-left: 6px', 'background: rgba(255,0,0,.4); color: white; padding: 4px 6px 4px 4px');
    } else if(logType === 'warning') {
        console.log('%c'+logTimestamp()+applicationPrefix+'%c'+msg,'background: rgba(255,165,0,.4); color: rgba(255,255,255,.4); font-size: 12px; line-height: 22px; padding-left: 6px', 'background: rgba(255,165,0,.4); color: white; padding: 4px 6px 4px 4px');
    } else if(logType === 'success') {
        console.log('%c'+logTimestamp()+applicationPrefix+'%c'+msg,'background: rgba(20,255,20,.4); color: rgba(255,255,255,.4); font-size: 12px; line-height: 22px; padding-left: 6px', 'background: rgba(20,255,20,.4); color: white; padding: 4px 6px 4px 4px');
    } else if(logType === 'info') {
        console.log('%c'+logTimestamp()+applicationPrefix+'%c'+msg,'background: rgba(60,160,255,.4); color: rgba(255,255,255,.4); font-size: 12px; line-height: 22px; padding-left: 6px', 'background: rgba(60,160,255,.4); color: white; padding: 4px 6px 4px 4px');
    } else {
        console.error('An internal error occured because the given logType was invalid.')
    }

}
