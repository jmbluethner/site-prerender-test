const SitePrerender = require('./lib/site-prerender/index');

const sp = new SitePrerender({
    staticPath: './src',
    outputFolder: './dist',
    routes: ['/']
})

async function exec() {
    await sp.init()
    await sp.start()
    await sp.close()
}

return exec();